
#include <iostream>
#include "picture.h"
#include "div.h"
#include "writers.h"

#include <stdio.h>
#include <list>
#include <string.h>
using namespace std;



int id = 0; //id global pt divuri


Hdiv findRect(int i, int j, picture pic, int(&check)[max][max])
{
	int x, y, maxi, maxj;
	rgbColor color;
	x = i;
	y = j;
	maxi = i; //i si j e coltul din st sus, iar maxi si maxj coltul din dreapta jos
	maxj = j;
	color.r = pic.pixels[i][j].r; //culoarea de la care pornim verificarea
	color.g = pic.pixels[i][j].g;
	color.b = pic.pixels[i][j].b;
	//cout << "ooooooooooooooo culori de verificat:" << color.r << color.g << color.b << endl;
	int ok = 1;
	id++;
	check[i][j] = id; //id-ul divului. dreptunghiul din matrice va avea id-ul in fiecare casuta
	do
	{   //check if current pixel has the same color as the initial one
		while (pic.pixels[x][y + 1].r == color.r && pic.pixels[x][y + 1].g == color.g && pic.pixels[x][y + 1].b == color.b)
		{
			y++;
			check[x][y] = id;
		}
		maxj = y;
		maxi = x;
		y = j;
		//if color is different, stop
		if (pic.pixels[x + 1][y].r != color.r || pic.pixels[x + 1][y].g != color.g || pic.pixels[x + 1][y].b != color.b) ok = 0;
		else x++;

	} while (ok);
	/*if (color.r == 1 && color.b == 1 && color.g == 1)
	{
		return 0;
	}*/
	Hdiv myDiv(i, j, id, color, maxj-j, maxi-i); //cream divul cu punctul de start, dimensiuni si culoare, si id

	return myDiv;

}

void iteratePic(picture pic)
{
	list <Hdiv> divs;
	list <Hdiv> ::iterator it;
	Hdiv holder;
	int check[max][max] = { 0 };

	for (int i = 0; i<pic.height; i++)
		for (int j = 0; j < pic.width; j++)
		{
			if (check[i][j] == 0) {
				divs.push_back(findRect(i, j, pic, check));
			}
		}
	cout << "\n nr divuri in lista: " << divs.size() << endl;
	for (int ii = 0; ii < 100; ii++)
	{
		for (int jj = 0; jj < 100; jj++)
		{
			cout << check[ii][jj]<<" ";
		}
		cout << endl;
	}
	char strDivs[10000] = "";
	char strStyles[100000] = "";


	
	for (it = divs.begin(); it != divs.end(); ++it)
	{
		holder = *it;
		strcpy(strDivs, writeDivToString(holder, strDivs));
		strcpy(strStyles, writeStyleToString(holder, strStyles));
		cout <<"\n div id: "<< holder.id << endl;
	}

	cout << "\n final divstring is:\n " << strDivs << endl;
	cout << "\n final style string is:\n " << strStyles << endl;
	createFile(strDivs, strStyles);
}

unsigned char* readBMP(char* filename, picture &myPic)
{
	int i;
	FILE* f = fopen(filename, "rb");
	unsigned char info[54];
	fread(info, sizeof(unsigned char), 54, f); // read the 54-byte header

											   // extract image height and width from header
	int width = *(int*)&info[18];
	int height = *(int*)&info[22];


	cout << "width is " << width << endl;
	cout << "height is " << height << endl;

	if (width < 0 || height < 0) {
		printf("error at reading size of image. Header of file might be corrupted\n");
		return 0;
	}

	int size = 3 * width * height + 3 * height; //overall size, including row padding
	unsigned char* data = new unsigned char[size]; // allocate 3 bytes per pixel
	fread(data, sizeof(unsigned char), size, f); // read the rest of the data at once
	fclose(f);

	myPic.height = height;
	myPic.width = width;

	//converts from bmp format (gbr) in RGB
	for (i = 0; i < size; i += 3)
	{
		unsigned char tmp = data[i];
		data[i] = data[i + 2];
		data[i + 2] = tmp;
	}
	int paddingWidth =width % 4;
	cout <<"padding width is:\n"<< paddingWidth;
	width = width * 3+ paddingWidth;
	int l = 0; //row counter
	int k = 0; //column counter
	 i = 0; //iterator that goes from start of data to end of it
	int j=0;
	//cout the image and puts the image in the matrix
	while (i < size)
	{
		if (j == width - paddingWidth)
		{
			i = i+paddingWidth; //skip the padding
			k++;
			j = 0;
			l = 0;
		}

		cout << (int)data[i] << ' ';
		if (j % 3 == 0 && i!=0) {
			myPic.pixels[height - k-1][l].r = (int)data[i]; //height- k -1 means that the rows have to be put upside down(.bmp storing)
			myPic.pixels[height - k-1][l].g = (int)data[i + 1];
			myPic.pixels[height - k-1][l].b = (int)data[i + 2];
			if ((i) % 3 == 0) cout << endl; //a pixel on one line
			l = l + 1;
			i = i + 3;
			j = j + 3;
		}
		if (i == 0)
		{
			myPic.pixels[height - k - 1][l].r = (int)data[i]; //height- k -1 means that the rows have to be put upside down(.bmp storing)
			myPic.pixels[height - k - 1][l].g = (int)data[i + 1];
			myPic.pixels[height - k - 1][l].b = (int)data[i + 2];
			if ((i) % 3 == 0) cout << endl; //a pixel on one line
			i = i + 3;
			j = j + 3;
			l++;
		}
		if ((i) % 3 == 0) cout << endl; //a pixel on one line

	
	}
	return data;
}

int main()
{
	picture myPic;

	unsigned char *lalala = readBMP("main.bmp", myPic);
	iteratePic(myPic);
	myPic.showPic();

	system("pause");
	return 0;
}