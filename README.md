# README #

 The Input of the program will take an image that consists of rectangles and based on this image with the .bmp extension, will generate a .html page, also including the stylesheet in it that will represent these shapes as div elements inside the page, keeping their position and colour.
 
### Abstract ###

A language that is a list of div elements. Every div element consists of the following variables: struct colour (int r, int g, int b), int x, int y (positions) , int width, int height(dimension). This is the minimum information necessary to generate the rectangles in the HTML page that will be identical with the ones from the picture.

### Technologies used###

* Windows 10 as OS
* Visual Studio
* C++

### Use Case ###

* save a .bmp file in the sae folder with the project
* compile & run