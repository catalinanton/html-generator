#include "picture.h"

char* writeDivToString(Hdiv div, char * divs)
{
	char buffer[16];
	sprintf(buffer, "%d", div.id);
//	printf("\n initial string of divs: %s \n ", divs);
	char beginElement[] = "<div id='div\0";
	char endElement[] = "'>\n"
		"</div>\n\0";
	char newDiv[100] = ""; //allocated size of div
	strcpy(newDiv, beginElement);
	strcat(newDiv, buffer);
	strcat(newDiv, endElement);

//	cout << "\n#######generated HTML\n" << newDiv;

	char tmp[32000] = "";
	strcpy(tmp, divs);
	strcat(tmp, newDiv);
	return tmp;
//	cout << "concatenated list of divs\n" << tmp;

}

char* writeStyleToString(Hdiv div, char * styles)
{
	char id[16];
	char r[20], g[20], b[20];
	char width[16];
	char height[16];
	char x[16];
	char y[16];

	//preparing info from the div object
	sprintf(id, "%d", div.id);
	sprintf(r, "%d", div.color.r);
	sprintf(g, "%d", div.color.g);
	sprintf(b, "%d", div.color.b);
	sprintf(width, "%d", div.width);
	sprintf(height, "%d", div.height);
	sprintf(x, "%d", div.x);
	sprintf(y, "%d", div.y);

//	printf("\n initial string of divs: %s \n ", styles);
	char beginElement[] = "*{\nmargin:0;\nborder:0;\n}\n\0";
	char endElement[100] = "#div";
	char newDiv[500] = ""; //allocated size of div style

	if (div.id == 1)
	{
		strcpy(newDiv, beginElement);
	};
	strcat(newDiv, "#div");
	strcat(newDiv, id);
	strcat(newDiv, "{\nposition:absolute;\nwidth: ");
	strcat(newDiv, width);
	strcat(newDiv, "px;\nheight: ");
	strcat(newDiv, height);
	strcat(newDiv, "px;\ntop:");
	strcat(newDiv, x);
	strcat(newDiv, "px;\nleft: ");
	strcat(newDiv, y);
	strcat(newDiv, "px;\nbackground-color:rgb(");
	strcat(newDiv, r);
	strcat(newDiv, ",");
	strcat(newDiv, g);
	strcat(newDiv, ",");
	strcat(newDiv, b);
	strcat(newDiv, ");\n}\n");


	//cout << "\n#######generated style\n" << newDiv;

	char tmp[32000] = "";
	strcpy(tmp, styles);
	strcat(tmp, newDiv);
	return tmp;
//	cout << "concatenated list of styles\n" << tmp;

}

void createFile(char* divs, char* styles)
{
	FILE* html = fopen("index.html", "w");
	char finalHtml[32000] = "";

	
	char beginHtml[] = "<!DOCTYPE html>\n"
		"<html>\n"
		"<head>\n"
		"<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n"
		"<style>\n";
	char closeStyleBeginBody[] = "</style>\n"
		"</head>\n"
		"<body>\n";
	char endHtml[]="</body>\n"
		"</html>\n\0";

	strcpy(finalHtml, beginHtml);
	strcat(finalHtml, styles);
	strcat(finalHtml, closeStyleBeginBody);
	strcat(finalHtml, divs);
	strcat(finalHtml, endHtml);
	fputs(finalHtml, html);
	fclose(html);
};