#ifndef WRITERS
#define WRITERS
#include <iostream>
#include <string>
#include "div.h"
using namespace std;

void createFile(char* divs, char* styles);
char* writeDivToString(Hdiv div, char* divs);
char* writeStyleToString(Hdiv div, char* styles);


#endif
