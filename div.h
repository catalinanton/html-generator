#ifndef HTML_DIV
#define HTML_DIV
struct rgbColor {
	int r;
	int g;
	int b;
};
class Hdiv
{
public:
	int x, y;
	int id;
	rgbColor color;
	int width;
	int height;
	Hdiv();
	Hdiv(int posX, int posY, int divId, rgbColor divColor, int divWidth, int divHeight);
};

#endif